<?php
/* Data */
$_SESSION['success'] = "";
$errors = array();
$dbname = "users";

/* MySQL login */
$host = "localhost";
$username = "root";
$password = "";
$conn = new mysqli($host, $username, $password);

/* Check connection */
if($conn->connect_error){
    die("Connection failed: " . $conn->connect_error);
}

$last_login = date('Y-m-d H:i:s');

/* Handle user registration */
if(isset($_POST['register'])){
    /* Grab form data */
    $username = mysqli_real_escape_string($conn, trim($_POST['username']));
    $password_1 = mysqli_real_escape_string($conn, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($conn, $_POST['password_2']);

    /* Check for empty fields */
    if(empty($username)){
        array_push($errors, "Username input field is empty.");
    }
    if(empty($password_1)){
        array_push($errors, "Password input fields are empty.");
    }
    
    /* Check if passwords match */
    if($password_1 != $password_2){
        array_push($errors, "The passwords do not match.");
    }

    /* If no errors are found, proceed with user registration */
    if(count($errors) == 0){
        $password = sha1($password_1);
        $unique_id = uniqid();
        $_SESSION['loggedin'] = false;

        $register_query = "INSERT INTO users(username, password, unique_id, last_login)
                           VALUES ('$username', '$password', '$unique_id', '$last_login')";

        /* Check if database exists */
        $db_exists = $conn->query("SHOW DATABASES LIKE '$dbname'");

        /* If it does, register */
        if($db_exists->num_rows > 0){
            $conn->select_db($dbname);
            mysqli_query($conn, $register_query);
            header('location: login.php');
        }

        /* If it doesn't, create it, then register */
        else{
            $conn->query("CREATE DATABASE $dbname");
            $conn->select_db($dbname);
            $createTable_query = "CREATE TABLE users (
                                  username VARCHAR(50),
                                  password VARCHAR(50),
                                  unique_id VARCHAR(50),
                                  last_login VARCHAR(50)
                                  )";
            mysqli_query($conn, $createTable_query);
            mysqli_query($conn, $register_query);
            header('location: login.php');
        }
    }
}

/* Handle user login */
if(isset($_POST['login'])){
    /* Grab form data */
    $username = mysqli_real_escape_string($conn, $_POST['username']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);

    /* Get password hash */
    $hashed_password = sha1($password);
    
    /* Check for empty fields */
    if(empty($username)){
        array_push($errors, "Username input field is empty.");
    }
    if(empty($password)){
        array_push($errors, "Password input field is empty.");
    }

    /* If no errors are found, proceed with login */
    if(count($errors) == 0){
        /* Check if database exists */
        $db_exists = $conn->query("SHOW DATABASES LIKE '$dbname'");

        /* If it does, proceed with login */
        if($db_exists->num_rows > 0){
            $conn->select_db($dbname);
            $stmt = $conn->prepare("SELECT * FROM users WHERE username = ?");
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();

            /* If user exists */
            if($result->num_rows == 1){
                $row = $result->fetch_assoc();

                /* Check if password is correct */
                if($hashed_password == $row['password']){
                    $_SESSION['loggedin'] = True;
                    $_SESSION['username'] = $username;
                    $_SESSION['success'] = "You are logged-in.";

                    /* Login successful, proceed to destination page */
                    header('location: destination.php');
                }
                else{
                    array_push($errors, "Incorrect password.");
                }
            }

            /* If user doesn't exist */
            else{
                array_push($errors, "User does not exist.");
            }
        }

        /* If it doesn't, ask for registration */
        else{
            array_push($errors, "Users database does not exist, please register a user.");
        }
    }

    /* If errors are found */
    else{
        array_push($errors, "Username or password are incorrect.");
    }
}

$conn->close();
?>