<?php
session_start();
if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header("Location: login.php");
    exit;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>You are logged-in!</title>
    </head>
    <body>
        <h1>You are logged-in!</h1>
        <div>
            <a href="logout.php">Logout</a>
        </div>
    </body>
</html>