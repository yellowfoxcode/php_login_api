<?php
session_start();
include('api-pdo.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
    </head>
    <body>
        <div>
            <h2>Login</h2>
        </div>
        <form method="post" action="login.php">
            <?php include('errors.php'); ?>
            <div>
                <label for="username">User: </label>
                <input type="text" id="username" name="username">
            </div>
            <div>
                <label for="password">Password: </label>
                <input type="password" id="password" name="password">
            </div>
            <div>
                <button type="submit" name="login">Login</button>
            </div>
            <div>
                <a href="register.php">Register</a>
            </div>
        </form>
    </body>
</html>