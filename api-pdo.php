<?php
/* Data */
$_SESSION['success'] = "";
$errors = array();
$dbname = "users";

/* Database credentials */
$server = "localhost";
$user = "root";
$pass = "";

/* Handle user registration */
if(isset($_POST['register'])){
    /* Grab form data and sanitize it */
    $username = htmlspecialchars($_POST['username'], ENT_QUOTES, 'UTF-8');
    $password_1 = htmlspecialchars($_POST['password_1'], ENT_QUOTES, 'UTF-8');
    $password_2 = htmlspecialchars($_POST['password_2'], ENT_QUOTES, 'UTF-8');

    /* Check for empty fields */
    if(empty($username)){
        $errors[] = "Username input field is empty.";
    }
    if(empty($password_1)){
        $errors[] = "Password input fields are empty.";
    }
    
    /* Check if passwords match */
    if($password_1 != $password_2){
        $errors[] = "The passwords do not match.";
    }

    /* If no errors are found, proceed with user registration */
    if(count($errors) == 0){
        try{
            /* Grab unique ID and hash the password */
            $unique_id = uniqid();
            $hashed_password = sha1($password_1);

            /* Connect to host and check if database exists */
            $conn = new PDO("mysql:host=$server", $user, $pass);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbs = $conn->query('SHOW DATABASES');
            $db_exists = false;
            foreach($dbs as $db){
                if($db[0] == $dbname){
                    $db_exists = true;
                    break;
                }
            }

            /* If it does't exist, create it */
            if(!$db_exists){
                $stmt = $conn->prepare("CREATE DATABASE $dbname");
                $stmt->execute();

                /* Close connection and reestabilish it with the new database */
                $conn = null;
                $conn = new PDO("mysql:host=$server;dbname=$dbname", $user, $pass);

                /* Create table */
                $stmt = $conn->prepare("CREATE TABLE users (
                                        username VARCHAR(255) NOT NULL,
                                        password VARCHAR(255) NOT NULL,
                                        unique_id VARCHAR(255) NOT NULL
                                        )");
                $stmt->execute();
            }

            /* If it exists, connect to it */
            else{
                $conn = null;
                $conn = new PDO("mysql:host=$server;dbname=$dbname", $user, $pass);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }

            /* Insert data and redirect user */
            $stmt = $conn->prepare("INSERT INTO users(username, password, unique_id) 
                                    VALUES (:username, :password, :unique_id)");
            $stmt->bindParam(':username', $username);
            $stmt->bindParam(':password', $hashed_password);
            $stmt->bindParam(':unique_id', $unique_id);
            $stmt->execute();

            header('location: login.php');
        }
        catch(PDOException $e){
            $errors[] = "Database error: " . $e->getMessage();
        }
    }
}

/* Handle user login */
if(isset($_POST['login'])){
    /* Grab form data */
    $username = htmlspecialchars($_POST['username'], ENT_QUOTES, 'UTF-8');
    $password = htmlspecialchars($_POST['password'], ENT_QUOTES, 'UTF-8');

    /* Check for empty fields */
    if(empty($username)){
        $errors[] = "Username input field is empty.";
    }
    if(empty($password)){
        $errors[] = "Password input field is empty";
    }

    /* If no errors ar found, proceed with user login */
    if(count($errors) == 0){
        try{
            /* Grab password hash */
            $hashed_password = sha1($password);

            /* Connect to host and check if database exists */
            $conn = new PDO("mysql:host=$server", $user, $pass);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbs = $conn->query('SHOW DATABASES');
            $db_exists = false;
            foreach($dbs as $db){
                if($db[0] == $dbname){
                    $db_exists = true;
                    break;
                }
            }

            /* If it doesn't exist */
            if(!$db_exists){
                $errors[] = "Database does not exist, please register a user.";
            }

            /* If it does, connect to it and proceed with login */
            else{
                try{
                    $conn = null;
                    $conn = new PDO("mysql:host=$server;dbname=$dbname", $user, $pass);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                    /* Check user credentials */
                    $stmt = $conn->prepare("SELECT password FROM users WHERE username = :username");
                    $stmt->bindParam(':username', $username);
                    $stmt->execute();

                    $result = $stmt->fetch(PDO::FETCH_ASSOC);

                    if($result && $result['password'] === sha1($password)){
                        $_SESSION['loggedin'] = True;
                        $_SESSION['username'] = $username;
                        $_SESSION['success'] = "You are logged-in.";

                        /* Login successful, proceed to destination page */
                        header('location: destination.php');
                    }
                    else{
                        $errors[] = "Incorrect username or password.";
                    }
                }
                catch(PDOException $e){
                    $errors[] = "Database error: " . $e->getMessage();
                }
            }
        }
        catch(PDOException $e){
            $errors[] = "Database error: " . $e->getMessage();
        }
    }
}
?>