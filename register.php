<?php
session_start();
include('api-pdo.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Register</title>
    </head>
    <body>
        <form method="post" action="register.php">
            <?php include('errors.php'); ?>
            <div>
                <h2>Register</h2>
            </div>
            <div>
                <label for="username">User: </label>
                <input type="text" id="username" name="username">
            </div>
            <div>
                <label for="password_1">Password: </label>
                <input type="password" id="password_1" name="password_1">
            </div>
            <div>
                <label for="password_2">Confirm password: </label>
                <input type="password" id="password_2" name="password_2">
            </div>
            <div>
                <button type="submit" id="register" name="register">Register</button>
            </div>
            <div>
                <a href="login.php">Already have an account? Login here!</a>
            </div>
        </form>
    </body>
</html>