## PHP Login API ##

**Information**

This repository hosts a user login, logout and registration template writen in PHP,
that is ready to be dragged and droped into a project, customized and used.

The code interacts with a MySQL instance running on the host machine, to manipulate 
a database called 'users', which will contain all login information.

The database contains the following: Username, Password, Unique-ID.

Username contains well... the username, as a string.
The password column contains a SHA-1 hash of the user's password.
The Unique-ID column contains a randomly generated ID for each user.

If the database doesn't exist, it will be created.

The 'destination.php' file, is where the user goes to after login,
and it cannot be accessed without being logged in by default (which is what we want).

The code is easy to understand, and comments sprinkled through it tells you exactly
what it is doing, so you can feel free to modify it to your needs.

**How it works**

'login.php' and 'register.php' are the main webpages, they communicate with 'api.php', 
which is where all the logic actually happens.

'api.php' handles the manipulation of user data to either register or login the user, it
also works in junction with 'errors.php' to provide feedback if the user does something
wrong. After the user successfuly logged in, and no errors where reported, 'api.php' will
finally send them to 'destination.php'.

'destination.php' is a simple page, telling you that you're logged in, and providing a link 
to 'logout.php', which runs some quick logic to logout the user by destroying the current
session. After that, you're sent back to the login page.

**How to start**

Making it work is simple, just clone this repository to wherever you want, start a MySQL
instance, for example, using XAMPP, then access 'register.php' and register a user.

By doing that, the database will be created, and fed with the first user, now you can either
log-in using that user through 'login.php', or register more users.

Remember to change the MySQL host, username and password inside 'api.php' to fit your MySQL 
setup. By default, the host is 'localhost', username 'root' and the password is blank.

You can also change the database name using the '$dbname' variable inside 'api.php'.